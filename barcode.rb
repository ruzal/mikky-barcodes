#!/usr/bin/env ruby
# -*- ruby -*-
# encoding: UTF-8 
require 'creek'
require 'csv'

def formatBarcode str
    res = str
    if (/"|'|>|<|&/.match(str))
      res = res.gsub(/"/,"\"\"")
      res = "\"#{res}\""
    end
    return res
end

def extractRowData row, unitedBarCode, res
    arr = row.to_a
    a = arr.first
    if a
        if a[1]
        unitedBarCode = formatBarcode a[1]
        end
    end

        b = arr.last
        if b
            code = formatBarcode "#{b[1]}"
            res  <<  ["#{unitedBarCode}",code]
        end

    return unitedBarCode,res
end


puts "Skopirujte fajl .xlsx v papku s programmoj. Napishite nazvanie fajla, (example.xlsx or example)"
#begin
xlsxFile = gets.chomp


if not xlsxFile.include?("xlsx")
    xlsxFile = "#{xlsxFile}.xlsx"
else 
    xlsxFile = "#{xlsxFile}"
end
csvFile = xlsxFile.gsub(/.xlsx/,".csv")
File.exists?(csvFile) ? File.delete(csvFile) : nil
book = Creek::Book.new xlsxFile, with_headers: true
#rescue 
    #puts "Ошибка чтения файла " + xlsxFile
#end
sheets = book.sheets
res = []
cntUnitedCodes = 0
unitedBarCode = ""

#begin
sheets.each do |sheet|
    sheet.rows.each do |row|
        next if row["A1"]
        if row["A2"]
            unitedBarCode = formatBarcode row["A2"]
        end
        unitedBarCode,res = extractRowData row, unitedBarCode, res
    end
end
#rescue
    #puts "Ошибка извлечения данных из таблицы, неизвестный формат"
#end 

#begin
headers = []
res.each do |key|
    headers << key
end

CSV.open("#{csvFile}","wb",:headers=>headers) do |row|
    row << ["Тип документа","ИНН участника оборота товаров"]
    row << ["Документ на формирование наборов","27715754065"]
    row << ["УИТУ","Вложеннный УИТ/УИТУ"]
end

CSV.open("#{csvFile}","a+",:headers=>headers,:quote_char=>'"',:force_quotes => true) do |row|
    res.each do |barcode|
        row << barcode
    end
end

#rescue
    #puts "Ошибка записи в #{csvFile} файл"
#end

puts "Fajl #{csvFile} sozdan i gotov k ispol'zovaniju"